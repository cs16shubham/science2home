package com.example.asus.sciencetohome;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class Login extends AppCompatActivity {


    ProgressDialog progressDialog;
    RequestQueue requestQueue;
    StringRequest stringRequest;
    String ps;
    AutoCompleteTextView email;
    EditText password;
    Button login;
    String urllogin="http://192.168.43.59/project2/login_submit.php";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);


        email=(AutoCompleteTextView)findViewById(R.id.emailid);
        password= (EditText) findViewById(R.id.password);
        login=(Button)findViewById(R.id.login);

        if (!SharedprefUser.getInstance(this).getStaffId().equals("null"))
        {
            Intent i=new Intent(this,MainActivity.class);
            startActivity(i);
            finish();
        }
        else
        {
            login.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                    if(email.getText().toString().length()==0 || email.getText().toString().matches(".*\\w\\*."))
                    {
                        email.setError("Empty field");
                        return;
                    }

                    if((email.getText().toString().length()!=0 && (!email.getText().toString().contains("@") || (!email.getText().toString().contains(".")))))
                    {
                        email.setError("Invalid password");
                        return;
                    }

                    if (password.getText().toString().length()<4 || password.getText().toString().matches(".*\\w\\*."))
                    {
                        password.setError("Invalid Password");
                        return;
                    }


                    requestQueue= Volley.newRequestQueue(Login.this);
                    stringRequest=new StringRequest(Request.Method.POST, urllogin, new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {

                            progressDialog.dismiss();
                            try {
                                JSONObject obj=new JSONObject(response);
                                if(obj.getString("response").toString().equals("-1"))
                                {
                                    Toast.makeText(Login.this,"Invalid email",Toast.LENGTH_LONG).show();
                                }
                                else
                                {
                                    Log.e("SHU",obj.getJSONObject("result").getString("staff_id"));

                                    SharedprefUser.getInstance(Login.this).savestaffid(obj.getJSONObject("result").getString("staff_id"));

                                    Intent i=new Intent(getApplicationContext(),MainActivity.class);
                                    startActivity(i);
                                    finish();

                                }



                                // Log.e("namste",obj12.getString("staff_id").toString());
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                            progressDialog.dismiss();
                            Toast.makeText(getApplicationContext(),""+error.toString(),Toast.LENGTH_SHORT).show();

                        }
                    }){
                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {
                            Map<String,String> mp=new HashMap<>();
                            mp.put("email",email.getText().toString());
                            mp.put("password",password.getText().toString());

                            return mp;
                        }
                    };

                    requestQueue.add(stringRequest);

                    progressDialog=ProgressDialog.show(Login.this,"Logging in","Please wait...",true);


                }
            });

        }


    }
}
