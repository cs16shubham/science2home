package com.example.asus.sciencetohome;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Asus on 20-12-2018.
 */

public class Adapter extends RecyclerView.Adapter<Adapter.MyViewHolder> {

    int year,month,day,year1,month1,day1;
    String final_selected_date,final_selected_date1;
    public static TextView updatedate;

    private Date dateslip= Calendar.getInstance().getTime();
    private SimpleDateFormat simpleDateFormat=new SimpleDateFormat("dd-MM-yyyy");
    Adapterupdate adapterupdate;
    RecyclerView recyclerView11;
    Button btncloseupdate;

    ProgressDialog progressDialog1,progressDialog2,progressDialog3;
    ArrayList<String> currentstatus = new ArrayList<String>();
    ArrayList<String> currentperson = new ArrayList<String>();
    ArrayList<String> nextstatus = new ArrayList<String>();
    ArrayList<String> nextperson = new ArrayList<String>();

    private AlertDialog alertDialog1,alertDialog12;
    public AlertDialog alertDialog;


    Button closeupdate;
    EditText addvendorname;
    public static TextView addvendordate;

    String vendor,addvendordate1,sn;
    String ud="",rm="";
    String urlupdate="http://192.168.43.59/project2/update_add.php";
    String urladdvendor="http://192.168.43.59/project2/vendor_add.php";

    public static List<Sliplistclass> list_items;
    private Context context;

    public Adapter(List<Sliplistclass> list_items,Context context) {
        this.context = context;
        this.list_items=list_items;
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_main,parent,false);

        MyViewHolder viewHolder=new MyViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        final Sliplistclass listitem=list_items.get(position);
        holder.slipno.setText("SLIP NO - "+listitem.getSlipno());
        holder.date.setText(listitem.getInitialdate());
        holder.customer.setText(listitem.getCustomer());
        holder.requirement.setText(listitem.getRequirement());
        holder.vendor.setText(listitem.getVendorname());
        holder.currentstatus.setText(listitem.getCurrent_status());

        holder.btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final ArrayList<updatelistclass> updates=new ArrayList<>();
                final AlertDialog.Builder builder=new AlertDialog.Builder(context);
                View view1=((MainActivity)context).getLayoutInflater().inflate(R.layout.popuplayoutmain,null);
                builder.setView(view1);
                builder.setTitle("Update");



                recyclerView11=(RecyclerView)view1.findViewById(R.id.recycleridupdate);
                RecyclerView.LayoutManager linearLayoutManager = new LinearLayoutManager(context);
                recyclerView11.setLayoutManager(linearLayoutManager);

                btncloseupdate=(Button)view1.findViewById(R.id.closeupdate);

                RequestQueue mRequestQueue;
                StringRequest mStringRequest;
                String url = "http://192.168.43.59/project2/Fetch_Updates.php";
                mRequestQueue = Volley.newRequestQueue(context);
                Log.e("SHUBHAM","Prasad");
                Log.e("list",listitem.getSlipno());
                mStringRequest=new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {

                        progressDialog1.dismiss();
                        Log.e("response",response.toString());
                        Log.e("SHUBHAM","Prasad");
                        try {
                            JSONArray jsonArray=new JSONArray(response);
                            for(int i=0;i<jsonArray.length();i++)
                            {
                                JSONObject jsonObject=jsonArray.getJSONObject(i);
                                updates.add(i,new updatelistclass(""+jsonObject.getString("DATE"),
                                        ""+jsonObject.getString("UPDATE"),""+jsonObject.getString("UPDATED_BY"),
                                        ""+jsonObject.getString("REMARKS"),""+jsonObject.getString("NEXT_PERSON"),
                                        ""+jsonObject.getString("NEXT_STATUS")));

                            }
                            adapterupdate=new Adapterupdate(context,updates);
                            recyclerView11.setAdapter(adapterupdate);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }



                    }
                },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Toast.makeText(context,""+error.toString(),Toast.LENGTH_SHORT).show();
                                progressDialog1.dismiss();

                            }
                        })
                {
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {

                        Map<String,String> map=new HashMap<>();
                        map.put("slip_no",listitem.getSlipno());
                        return map;
                    }
                };
                mRequestQueue.add(mStringRequest);
                alertDialog=builder.create();
                alertDialog.show();
                progressDialog1=ProgressDialog.show(context,"Loading...","Please wait...",true);

                btncloseupdate.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        alertDialog.dismiss();
                    }
                });

            }
        });

        holder.addvendorbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                AlertDialog.Builder builder12 = new AlertDialog.Builder(context);
                builder12.setTitle("Add Slip");
                View v = ((MainActivity)context).getLayoutInflater().inflate(R.layout.addvendorlayout, null);
                builder12.setView(v);

               Button vendoradd=(Button)v.findViewById(R.id.vendoradd);
                 addvendorname=(EditText)v.findViewById(R.id.addvendorvendor);
                 addvendordate=(TextView)v.findViewById(R.id.addvendordate);

                final Spinner assignedby1=(Spinner)v.findViewById(R.id.assignedby);

                addvendorname.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void afterTextChanged(Editable editable) {
                        vendor=addvendorname.getText().toString();
                    }
                });

//                addvendordate.addTextChangedListener(new TextWatcher() {
//                    @Override
//                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//
//                    }
//
//                    @Override
//                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//
//                    }
//
//                    @Override
//                    public void afterTextChanged(Editable editable) {
//                            addvendordate1=addvendordate.getText().toString();
//                    }
//                });

                addvendordate.setText(simpleDateFormat.format(dateslip));

                addvendordate.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        ((MainActivity) context).showDialog(0);
                        MainActivity.ids=2;
                    }
                });

//                Calendar c=Calendar.getInstance();
//                 year=c.get(Calendar.YEAR);
//                 month=c.get(Calendar.MONTH);
//                day=c.get(Calendar.DAY_OF_MONTH);
//
//
//
//                DatePickerDialog.OnDateSetListener datepickerlistener=new DatePickerDialog.OnDateSetListener() {
//                    @Override
//                    public void onDateSet(DatePicker datePicker, int selectedyear, int selectedmonth, int selectedday)
//                    {
//                        day=selectedday;
//                        month=selectedmonth;
//                        year=selectedyear;
//
//                        final_selected_date=selectedyear+"-"+(selectedmonth+1)+"-"+selectedday;
//
//
//                        try {
//                            addvendordate.setText(new SimpleDateFormat("dd-MM-yyyy").format(new SimpleDateFormat("yyyy-MM-dd").parse(final_selected_date)));
//                        } catch (ParseException e) {
//                            e.printStackTrace();
//                        }
//                    }
//                };






                ArrayList<String> currentp = new ArrayList<String>();
                currentp.add(0,"CURRENT PERSON");
                ArrayAdapter<String> currentperson_adapter1 = new ArrayAdapter<>(context, android.R.layout.simple_spinner_dropdown_item,currentp);
                String cp1=Sharedpref.getInstance(context).getJSONArrayperson();
                Log.e("cp",cp1);
                String staffid=SharedprefUser.getInstance(context).getStaffId();

                try {
                    JSONArray array13=new JSONArray(cp1);
                    Log.e("length",String.valueOf(array13.length()));
                    for(int i=0;i<array13.length();i++)
                    {
                        JSONObject jsonObject11=array13.getJSONObject(i);
                        //currentp.add(i+1,jsonObject11.getString("name"));

                        if(staffid.equals(jsonObject11.getString("staff_id")))
                        {
                            currentp.add(1,jsonObject11.getString("name"));
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                addvendordate1=addvendordate.getText().toString();
                currentperson_adapter1.notifyDataSetChanged();

                currentperson_adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                assignedby1.setAdapter(currentperson_adapter1);
//
//
                alertDialog12=builder12.create();
                alertDialog12.show();

                vendoradd.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                            final String person=assignedby1.getSelectedItem().toString();
                            sn=listitem.getSlipno().toString();

                            Log.e("person",person);
                            Log.e("date",addvendordate1);
                            Log.e("vendor",vendor);
                            Log.e("slipno",sn);


                            RequestQueue mrequestvendor;
                            StringRequest mStringrequestvendor;

                            mrequestvendor=Volley.newRequestQueue(context);
                            mStringrequestvendor=new StringRequest(Request.Method.POST, urladdvendor, new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    Log.e("res",response.toString());

                                    Intent i=new Intent(context,MainActivity.class);
                                    context.startActivity(i);
                                    alertDialog12.cancel();

                                }
                            }, new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {

                                }
                            }){
                                @Override
                                protected Map<String, String> getParams() throws AuthFailureError {
                                    Map<String,String> mp=new HashMap<>();
                                    mp.put("slip_no",sn);
                                    mp.put("vendor",vendor);
//                                    mp.put("date",addvendordate1)
                                    try {
                                        mp.put("date",new SimpleDateFormat("yyyy-MM-dd").format(new SimpleDateFormat("dd-MM-yyyy").parse(ud)));
                                    } catch (ParseException e) {
                                        e.printStackTrace();
                                    }
                                    mp.put("assigned_by",person);
                                    return mp;
                                }
                            };
                            mrequestvendor.add(mStringrequestvendor);
                    }
                });




            }
        });



        holder.btnupdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                final AlertDialog.Builder builder1=new AlertDialog.Builder(context);
                View view11=((MainActivity)context).getLayoutInflater().inflate(R.layout.updatepopup,null);
                builder1.setView(view11);
                builder1.setTitle(" Add Update");





                final Button updateclose,updateadd;
                final EditText remark;
                final Spinner updatecurrentstatus,updatenextstatus,updatecurrentperson,updatenextperson;
                updatedate=(TextView) view11.findViewById(R.id.updatedate);
                updatecurrentperson=(Spinner)view11.findViewById(R.id.updatecurrentperson);
                updatenextperson=(Spinner)view11.findViewById(R.id.updatenextperson);
                updatecurrentstatus=(Spinner)view11.findViewById(R.id.updatecurrentstatus);
                updatenextstatus=(Spinner)view11.findViewById(R.id.updatenextstatus);
                updateclose=(Button)view11.findViewById(R.id.updateclose);
                updateadd=(Button)view11.findViewById(R.id.updateadd);
                remark=(EditText)view11.findViewById(R.id.updateremark);






                updateadd.setOnClickListener(new View.OnClickListener() {



                    @Override
                    public void onClick(View view) {

                        ud=updatedate.getText().toString();

                        if (ud.length()==0)
                        {
                            updatedate.setError("Empyt field");
                            return;
                        }


                        final String slipno=listitem.getSlipno();
                        final String cs=updatecurrentstatus.getSelectedItem().toString();
                        final String ns=updatenextstatus.getSelectedItem().toString();
                        final String cp=updatecurrentperson.getSelectedItem().toString();
                        final String np=updatenextperson.getSelectedItem().toString();
                        RequestQueue mrequest;
                        StringRequest mstringrequest;

                        mrequest=Volley.newRequestQueue(context);
                        mstringrequest=new StringRequest(Request.Method.POST, urlupdate, new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                Log.e("TRUE",response.toString());

                                alertDialog1.dismiss();

                                Intent i=new Intent(context,MainActivity.class);
                                context.startActivity(i);

                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {

                            }
                        })
                        {
                            @Override
                            protected Map<String, String> getParams() throws AuthFailureError {


                                Map<String,String> map=new HashMap<>();

                                map.put("slip_no",slipno);


                                try {
                                    map.put("date",new SimpleDateFormat("yyyy-MM-dd").format(new SimpleDateFormat("dd-MM-yyyy").parse(ud)));
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                                if(cs.equals("CURRENT STATUS"))
                                {
                                    map.put("current_status","--");
                                }
                                else
                                {
                                    map.put("current_status",cs);
                                }

                                if(ns.equals("NEXT STATUS"))
                                {
                                    map.put("next_status","--");
                                }
                                else
                                {
                                    map.put("next_status",ns);
                                }

                                if (cp.equals("CURRENT PERSON"))
                                {
                                    map.put("current_perosn","--");
                                }
                                else
                                {
                                    map.put("current_person",cp);
                                }
                                if(np.equals("NEXT PERSON"))
                                {
                                    map.put("next_person","--");
                                }
                                else
                                {
                                    map.put("next_person",np);
                                }

                                if (rm.length()==0)
                                {
                                    map.put("remarks",rm);
                                }
                                else
                                {
                                    map.put("remarks",rm);
                                }



                                return map;
                            }
                        };
                        mrequest.add(mstringrequest);
                    }
                });
//                updatedate.addTextChangedListener(new TextWatcher() {
//                    @Override
//                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//
//                    }
//
//                    @Override
//                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//
//                    }
//
//                    @Override
//                    public void afterTextChanged(Editable editable) {
//                          ud=updatedate.getText().toString();
//
//                    }
//                });
                final_selected_date1=new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());

                updatedate.setText(simpleDateFormat.format(dateslip));
                updatedate.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        ((MainActivity) context).showDialog(0);
                        MainActivity.ids=1;
                    }
                });





                remark.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void afterTextChanged(Editable editable) {
                        rm=remark.getText().toString();
                    }
                });



                currentstatus.add(0,"CURRENT STATUS");
                final ArrayAdapter<String> currentstatus_adapter = new ArrayAdapter<>(context, android.R.layout.simple_spinner_dropdown_item,currentstatus);
                 String cs1=Sharedpref.getInstance(context).getJSONArraycurrent();
                 Log.e("cs",cs1);

                try {
                    JSONArray array=new JSONArray(cs1);
                    for(int i=0;i<array.length();i++)
                    {
                        JSONObject jsonObject=array.getJSONObject(i);
                        currentstatus.add(i+1,jsonObject.getString("STATUS"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                currentstatus_adapter.notifyDataSetChanged();

                currentstatus_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                updatecurrentstatus.setAdapter(currentstatus_adapter);



                nextstatus.add(0,"NEXT STATUS");
                final ArrayAdapter<String> nextstatus_adapter = new ArrayAdapter<>(context, android.R.layout.simple_spinner_dropdown_item,nextstatus);
                String ns1=Sharedpref.getInstance(context).getJSONArraynext();
                Log.e("ns",ns1);

                try {
                    JSONArray array1=new JSONArray(ns1);
                    Log.e("length",String.valueOf(array1.length()));
                    for(int i=0;i<array1.length();i++)
                    {
                        JSONObject jsonObject1=array1.getJSONObject(i);
                        nextstatus.add(i+1,jsonObject1.getString("STATUS"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                nextstatus_adapter.notifyDataSetChanged();

                nextstatus_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                updatenextstatus.setAdapter(nextstatus_adapter);



                currentperson.add(0,"CURRENT PERSON");
                final ArrayAdapter<String> currentperson_adapter = new ArrayAdapter<>(context, android.R.layout.simple_spinner_dropdown_item,currentperson);
                String cp1=Sharedpref.getInstance(context).getJSONArrayperson();
                String staffid=SharedprefUser.getInstance(context).getStaffId();

                try {
                    JSONArray array13=new JSONArray(cp1);
                    Log.e("length",String.valueOf(array13.length()));
                    for(int i=0;i<array13.length();i++)
                    {
                        JSONObject jsonObject11=array13.getJSONObject(i);
                        //currentp.add(i+1,jsonObject11.getString("name"));

                        if(staffid.equals(jsonObject11.getString("staff_id")))
                        {
                            currentperson.add(1,jsonObject11.getString("name"));
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                currentperson_adapter.notifyDataSetChanged();

                currentperson_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                updatecurrentperson.setAdapter(currentperson_adapter);



                nextperson.add(0,"NEXT PERSON");
                final ArrayAdapter<String> nextperson_adapter = new ArrayAdapter<>(context, android.R.layout.simple_spinner_dropdown_item,nextperson);
                String np1=Sharedpref.getInstance(context).getJSONArrayperson();
                Log.e("np",np1);

                try {
                    JSONArray array111=new JSONArray(np1);
                    Log.e("length",String.valueOf(array111.length()));
                    for(int i=0;i<array111.length();i++)
                    {
                        JSONObject jsonObject111=array111.getJSONObject(i);
                        nextperson.add(i+1,jsonObject111.getString("name"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                nextperson_adapter.notifyDataSetChanged();

                nextperson_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                updatenextperson.setAdapter(nextperson_adapter);




                alertDialog1=builder1.create();
                alertDialog1.show();
                //progressDialog2=ProgressDialog.show(context,"Loading...","Please wait...",true);



                updateclose.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        alertDialog1.dismiss();
                    }
                });
            }


        });



    }

    @Override
    public int getItemCount() {
        return list_items.size();
    }
    public class MyViewHolder extends RecyclerView.ViewHolder{

        private TextView slipno,date,customer,requirement,vendor,currentstatus;
        private Button btn,btnupdate,addvendorbutton;


        public MyViewHolder(View itemView) {
            super(itemView);


            currentstatus=(TextView)itemView.findViewById(R.id.actualcurrentstatus);
            slipno=(TextView)itemView.findViewById(R.id.slipno);
            date=(TextView)itemView.findViewById(R.id.actualdate);
            customer=(TextView)itemView.findViewById(R.id.actualcustomername);
            requirement=(TextView)itemView.findViewById(R.id.actualrequirement);
            vendor=(TextView)itemView.findViewById(R.id.actualvendorname);
            btn=(Button)itemView.findViewById(R.id.checkupdatebutton);
            btnupdate=(Button)itemView.findViewById(R.id.btnupdate);
            addvendorbutton=(Button)itemView.findViewById(R.id.btnvendor);


        }


    }
}
