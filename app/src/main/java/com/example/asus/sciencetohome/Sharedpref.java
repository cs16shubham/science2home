package com.example.asus.sciencetohome;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by THE Ajnur on 13-Dec-18.
 */

public class Sharedpref{

    private static final String SHARED_PREF_NAME = "slipnumber";
    private static final String SLIP_NO="slip_no";
    private static final String JSONArrayperson="jsonarrayperson";
    private static final String JSONArraycurrent="jsonarraycurrent";
    private static final String JSONArraynext="jsonarraynext";

    //private static final String TEACHER_ATTENDANCE="teacher_attendance";

    //Instance and Context declarations

    private static Sharedpref mInstance;
    private static Context mCtx;
    //private static final String STUDENT_ID="student_id";

    //Private Constructor
    private Sharedpref(Context context) {
        mCtx = context;
    }

    //Public Method calling Private Constructor
    public static synchronized Sharedpref getInstance(Context context) {
        if (mInstance == null)
        {
            mInstance = new Sharedpref(context);
        }
        return mInstance;
    }
    public void saveslipno(String token) {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(SLIP_NO, token);

        editor.commit();
        editor.apply();
    }

    public void savearrayperson(String json) {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(JSONArrayperson, json);

        editor.commit();
        editor.apply();
    }

    public String getJSONArrayperson()
    {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getString(JSONArrayperson,"null");

    }

    public void savearraycurrent(String json) {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(JSONArraycurrent, json);

        editor.commit();
        editor.apply();
    }

    public String getJSONArraycurrent()
    {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getString(JSONArraycurrent,"null");

    }

    public void savearraynext(String json) {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(JSONArraynext, json);

        editor.commit();
        editor.apply();
    }

    public String getJSONArraynext()
    {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getString(JSONArraynext,"null");

    }


    public String getSlipNo()
    {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getString(SLIP_NO,"null");

    }
}
