package com.example.asus.sciencetohome;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Asus on 24-12-2018.
 */

public class SharedprefUser {

    private static final String SHARED_PREF_NAME = "slipnumber";
    private static final String STAFF_ID="staffid";

    private static SharedprefUser mInstance;
    private static Context mCtx;
    //private static final String STUDENT_ID="student_id";

    //Private Constructor
    private SharedprefUser(Context context) {
        mCtx = context;
    }

    //Public Method calling Private Constructor
    public static synchronized SharedprefUser getInstance(Context context) {
        if (mInstance == null)
        {
            mInstance = new SharedprefUser(context);
        }
        return mInstance;
    }
    public void savestaffid(String staffid)
    {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(STAFF_ID,staffid);

        editor.commit();
        editor.apply();
    }
    public void logout()
    {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.clear();
        editor.apply();
    }
    public String getStaffId()
    {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getString(STAFF_ID,"null");

    }


}
