package com.example.asus.sciencetohome;

/**
 * Created by Asus on 20-12-2018.
 */

public class Sliplistclass {

    private String slipno,initialdate,customer,current_status,requirement,vendorname;

    public Sliplistclass(String slipno, String initialdate, String customer, String current_status, String requirement, String vendorname) {
        this.slipno = slipno;
        this.initialdate = initialdate;
        this.customer = customer;
        this.current_status = current_status;
        this.requirement = requirement;
        this.vendorname = vendorname;
    }





    public String getSlipno() {
        return slipno;
    }

    public void setSlipno(String slipno) {
        this.slipno = slipno;
    }

    public String getInitialdate() {
        return initialdate;
    }

    public void setInitialdate(String initialdate) {
        this.initialdate = initialdate;
    }

    public String getCustomer() {
        return customer;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }

    public String getCurrent_status() {
        return current_status;
    }

    public void setCurrent_status(String current_status) {
        this.current_status = current_status;
    }

    public String getRequirement() {
        return requirement;
    }

    public void setRequirement(String requirement) {
        this.requirement = requirement;
    }

    public String getVendorname() {
        return vendorname;
    }

    public void setVendorname(String vendorname) {
        this.vendorname = vendorname;
    }
}
