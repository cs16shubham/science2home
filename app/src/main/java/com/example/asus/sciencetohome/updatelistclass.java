package com.example.asus.sciencetohome;

/**
 * Created by Asus on 22-12-2018.
 */

public class updatelistclass {

    private String date,update,updateby,remarks,nextperson,nextstatus;

    public updatelistclass(String date, String update, String updateby, String remarks, String nextperson, String nextstatus) {
        this.date = date;
        this.update = update;
        this.updateby = updateby;
        this.remarks = remarks;
        this.nextperson = nextperson;
        this.nextstatus = nextstatus;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getUpdate() {
        return update;
    }

    public void setUpdate(String update) {
        this.update = update;
    }

    public String getUpdateby() {
        return updateby;
    }

    public void setUpdateby(String updateby) {
        this.updateby = updateby;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getNextperson() {
        return nextperson;
    }

    public void setNextperson(String nextperson) {
        this.nextperson = nextperson;
    }

    public String getNextstatus() {
        return nextstatus;
    }

    public void setNextstatus(String nextstatus) {
        this.nextstatus = nextstatus;
    }
}
