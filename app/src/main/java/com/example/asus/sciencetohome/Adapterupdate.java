package com.example.asus.sciencetohome;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.List;

/**
 * Created by Asus on 22-12-2018.
 */

   public class Adapterupdate extends RecyclerView.Adapter<Adapterupdate.MyViewHolder> {

    public static List<updatelistclass> update_item;
    private Context context;

    public Adapterupdate(Context context,List<updatelistclass> update_item) {
        this.context = context;
        this.update_item=update_item;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v= LayoutInflater.from(parent.getContext()).inflate(R.layout.popuplayout,parent,false);

        MyViewHolder viewHolder=new MyViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        updatelistclass updateitem=update_item.get(position);

        holder.actualdate.setText(updateitem.getDate());
        holder.actualupdate.setText(updateitem.getUpdate());
        holder.actualupdatedby.setText(updateitem.getUpdateby());
        holder.actualremarks.setText(updateitem.getRemarks());
        holder.actualnextstatus.setText(updateitem.getNextstatus());
        holder.actualnextperson.setText(updateitem.getNextperson());

        holder.date.setText("Date");
        holder.update.setText("Update");
        holder.updatedby.setText("Updated by");
        holder.remarks.setText("Remarks");
        holder.nextstatus.setText("Next status");
        holder.nextperson.setText("Next person");

    }

    @Override
    public int getItemCount() {
        return update_item.size();
    }




    public class MyViewHolder extends RecyclerView.ViewHolder{

        private TextView date,actualdate,update,actualupdate,
                updatedby,actualupdatedby,remarks,actualremarks,
                nextstatus,actualnextstatus,nextperson,actualnextperson;

       // private Button closeupdate;

        public MyViewHolder(View itemView) {
            super(itemView);

            date=(TextView)itemView.findViewById(R.id.date);
            actualdate=(TextView)itemView.findViewById(R.id.actualdate);
            update=(TextView)itemView.findViewById(R.id.update);
            actualupdate=(TextView)itemView.findViewById(R.id.actualupdate);
            updatedby=(TextView)itemView.findViewById(R.id.updateby);
            actualupdatedby=(TextView)itemView.findViewById(R.id.actualupdateby);
            remarks=(TextView)itemView.findViewById(R.id.remarks);
            actualremarks=(TextView)itemView.findViewById(R.id.actualremarks);
            nextstatus=(TextView)itemView.findViewById(R.id.nextstatus);
            actualnextstatus=(TextView)itemView.findViewById(R.id.actualnextstatus);
            nextperson=(TextView)itemView.findViewById(R.id.nextperson);
            actualnextperson=(TextView)itemView.findViewById(R.id.actualnextperson);
         //   closeupdate=(Button)itemView.findViewById(R.id.closeupdate);
        }
    }
}
