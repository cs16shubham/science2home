package com.example.asus.sciencetohome;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;





   public class MainActivity extends AppCompatActivity {

       private Date dateslip= Calendar.getInstance().getTime();
       private SimpleDateFormat simpleDateFormat=new SimpleDateFormat("dd-MM-yyyy");



    ArrayList<Sliplistclass> students=new ArrayList<>();

    ArrayList<String> currentstatus = new ArrayList<String>();
    ArrayList<String> currentperson = new ArrayList<String>();
    ArrayList<String> nextstatus = new ArrayList<String>();
    ArrayList<String> nextperson = new ArrayList<String>();
    public static int ids=0;

        TextView date;

        public  static  String final_selected_date;


//        int year,month,day;
        EditText slipno,client,requirement,vendor;
       Spinner current_person,next_status,next_person,current_status;
       Button add;

       EditText addvendorname,addvendordate;
       Spinner assignedby1;
       Button vendoradd;

    AlertDialog alertDialog11,alertDialog12;
    String asn,asc,asd,asr,asv,ascs,asns,asnp;
    String txt="";
    private RecyclerView recyclerView;
    private EditText editText;
    private Button addslipbutton,logout;

    private Spinner personspinner,currentstatusspinner,nextstatusspinner;

    private RequestQueue mRequestQueue,mRequestqueueperson,mRequestqueuestatus,mRequestqueuenext,mRequestqueuesearch,mRequestaddslip;
    private StringRequest mStringRequest,mStringRequestperson,mStringRequeststatus,mStringRequestnext,mStringRequestsearch,mStringaddslip;
    private Adapter adapter;
    private String url      ="http://192.168.43.59/project2/Fetch.php";
    private String urlperson="http://192.168.43.59/project2/person_fill.php";
    private String urlstatus="http://192.168.43.59/project2/status_fill.php";
    private String urlsearch="http://192.168.43.59/project2/Search.php";
    private String urladdslip="http://192.168.43.59/project2/client_add.php";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.realactivitymain);


        addslipbutton=(Button)findViewById(R.id.btnaddslip);
        logout=(Button)findViewById(R.id.logout);
        editText=(EditText)findViewById(R.id.search);
        final_selected_date=new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());

       // date.setText((CharSequence) dateslip);

        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedprefUser.getInstance(getApplicationContext()).logout();
                startActivity(new Intent(MainActivity.this,Login.class));
                finish();
            }
        });
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {


                final String currentstatus=currentstatusspinner.getSelectedItem().toString();
                final String nextstatus=nextstatusspinner.getSelectedItem().toString();
                final String nextperson=personspinner.getSelectedItem().toString();

                txt=editText.getText().toString();
                Log.e("Post",currentstatus+nextperson+nextstatus+txt);




                Log.e("Post",currentstatus+nextperson+nextstatus+txt);




                //  RequestQueue mRequestQueue;
                //StringRequest mStringRequest;
                //String url = "http://192.168.43.59/project2/Fetch_Updates.php";
                mRequestqueuesearch = Volley.newRequestQueue(MainActivity.this);
                Log.e("SHUBHAM","Prasad");
                // Log.e("list",listitem.getSlipno());
                mStringRequestsearch=new StringRequest(Request.Method.POST, urlsearch, new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {
                        Log.e("response",response.toString());
                        Log.e("SHUBHAM","Prasad");
                        students.clear();
                        //               try {
//                    JSONArray jsonArray=new JSONArray(response);
//                    for(int i=0;i<jsonArray.length();i++)
//                    {
//                        JSONObject jsonObject=jsonArray.getJSONObject(i);
//                        date.setText(jsonObject.getString("DATE"));
//                        update.setText(jsonObject.getString("UPDATE"));
//                        updated_By.setText(jsonObject.getString("UPDATED_BY"));
//                        remarks.setText(jsonObject.getString("REMARKS"));
//                        next_person.setText(jsonObject.getString("NEXT_PERSON"));
//                        next_status.setText(jsonObject.getString("NEXT_STATUS"));
//                    }
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
                        try {

                            JSONArray array11=new JSONArray(response);
                            for(int i=0;i<array11.length();i++)
                            {
                                JSONObject jsonObject11=array11.getJSONObject(i);
                                students.add(i,new Sliplistclass(""+jsonObject11.getString("SLIP_NO"),""+jsonObject11.getString("START_DATE"),""+jsonObject11.getString("CLIENT"),""+jsonObject11.getString("STATUS"),""+jsonObject11.getString("REQUIREMENT"),""+jsonObject11.getString("VENDOR_NAME")));
                            }
                            adapter=new Adapter(students,MainActivity.this);
                            recyclerView.setAdapter(adapter);
                            // Sharedpref.getInstance(getApplicationContext()).saveslipno(jsonObject1.getString("token"));




                        } catch (JSONException e) {
                            //  Log.e("sss","hh");
                            e.printStackTrace();
                        }




                    }
                },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Log.e("SHUBHAM",error.toString());

                            }
                        })
                {
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        Log.e("Post",currentstatus+nextperson+nextstatus+txt);

                        Map<String,String> map=new HashMap<>();


                        if(txt.equals("0"))
                        {
                            Log.e("txt","0");
                            map.put("client","");
                        }
                        else
                        {
                            Log.e("client",txt);
                            map.put("client",txt);
                        }

                        if(currentstatus.equals("CURRENT STATUS"))
                        {
                            map.put("current_status","0");
                            Log.e("current_status","0");
                        }
                        else
                        {
                            map.put("current_status",currentstatus);
                            Log.e("current_status",currentstatus);
                        }

                        if(nextstatus.equals("NEXT STATUS"))
                        {

                            map.put("next_status","0");
                            Log.e("next_status","0");
                        }
                        else {
                            Log.e("next_status",nextstatus);
                            map.put("next_status",nextstatus);
                        }

                        if(nextperson.equals("PERSON"))
                        {
                            Log.e("next_person","0");
                            map.put("next_person","0");
                        }
                        else
                        {
                            Log.e("next_person",nextperson);
                            map.put("next_person",nextperson);
                        }


                        return map;
                    }
                };
                mRequestqueuesearch.add(mStringRequestsearch);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });



        addslipbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addslip();
            }
        });


        personspinner=(Spinner)findViewById(R.id.personspinner);
        currentstatusspinner=(Spinner)findViewById(R.id.currentspinner);

        nextstatusspinner=(Spinner)findViewById(R.id.nextspinner);
        personspinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                final String currentstatus=currentstatusspinner.getSelectedItem().toString();
                final String nextstatus=nextstatusspinner.getSelectedItem().toString();
                final String nextperson=personspinner.getSelectedItem().toString();

                txt=editText.getText().toString();
                Log.e("Post",currentstatus+nextperson+nextstatus+txt);




                Log.e("Post",currentstatus+nextperson+nextstatus+txt);




                //  RequestQueue mRequestQueue;
                //StringRequest mStringRequest;
                //String url = "http://192.168.43.59/project2/Fetch_Updates.php";
                mRequestqueuesearch = Volley.newRequestQueue(MainActivity.this);
                Log.e("SHUBHAM","Prasad");
                // Log.e("list",listitem.getSlipno());
                mStringRequestsearch=new StringRequest(Request.Method.POST, urlsearch, new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {
                        Log.e("response",response.toString());
                        Log.e("SHUBHAM","Prasad");
                        students.clear();
                        //               try {
//                    JSONArray jsonArray=new JSONArray(response);
//                    for(int i=0;i<jsonArray.length();i++)
//                    {
//                        JSONObject jsonObject=jsonArray.getJSONObject(i);
//                        date.setText(jsonObject.getString("DATE"));
//                        update.setText(jsonObject.getString("UPDATE"));
//                        updated_By.setText(jsonObject.getString("UPDATED_BY"));
//                        remarks.setText(jsonObject.getString("REMARKS"));
//                        next_person.setText(jsonObject.getString("NEXT_PERSON"));
//                        next_status.setText(jsonObject.getString("NEXT_STATUS"));
//                    }
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
                        try {

                            JSONArray array11=new JSONArray(response);
                            for(int i=0;i<array11.length();i++)
                            {
                                JSONObject jsonObject11=array11.getJSONObject(i);
                                students.add(i,new Sliplistclass(""+jsonObject11.getString("SLIP_NO"),""+jsonObject11.getString("START_DATE"),""+jsonObject11.getString("CLIENT"),""+jsonObject11.getString("STATUS"),""+jsonObject11.getString("REQUIREMENT"),""+jsonObject11.getString("VENDOR_NAME")));
                            }
                            adapter=new Adapter(students,MainActivity.this);
                            recyclerView.setAdapter(adapter);
                            // Sharedpref.getInstance(getApplicationContext()).saveslipno(jsonObject1.getString("token"));




                        } catch (JSONException e) {
                            //  Log.e("sss","hh");
                            e.printStackTrace();
                        }




                    }
                },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Log.e("SHUBHAM",error.toString());

                            }
                        })
                {
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        Log.e("Post",currentstatus+nextperson+nextstatus+txt);

                        Map<String,String> map=new HashMap<>();


                        if(txt.equals("0"))
                        {
                            Log.e("txt","0");
                            map.put("client","");
                        }
                        else
                        {
                            Log.e("client",txt);
                            map.put("client",txt);
                        }

                        if(currentstatus.equals("CURRENT STATUS"))
                        {
                            map.put("current_status","0");
                            Log.e("current_status","0");
                        }
                        else
                        {
                            map.put("current_status",currentstatus);
                            Log.e("current_status",currentstatus);
                        }

                        if(nextstatus.equals("NEXT STATUS"))
                        {

                            map.put("next_status","0");
                            Log.e("next_status","0");
                        }
                        else {
                            Log.e("next_status",nextstatus);
                            map.put("next_status",nextstatus);
                        }

                        if(nextperson.equals("PERSON"))
                        {
                            Log.e("next_person","0");
                            map.put("next_person","0");
                        }
                        else
                        {
                            Log.e("next_person",nextperson);
                            map.put("next_person",nextperson);
                        }


                        return map;
                    }
                };
                mRequestqueuesearch.add(mStringRequestsearch);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        nextstatusspinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                final String currentstatus=currentstatusspinner.getSelectedItem().toString();
                final String nextstatus=nextstatusspinner.getSelectedItem().toString();
                final String nextperson=personspinner.getSelectedItem().toString();

                txt=editText.getText().toString();
                Log.e("Post",currentstatus+nextperson+nextstatus+txt);




                Log.e("Post",currentstatus+nextperson+nextstatus+txt);




                //  RequestQueue mRequestQueue;
                //StringRequest mStringRequest;
                //String url = "http://192.168.43.59/project2/Fetch_Updates.php";
                mRequestqueuesearch = Volley.newRequestQueue(MainActivity.this);
                Log.e("SHUBHAM","Prasad");
                // Log.e("list",listitem.getSlipno());
                mStringRequestsearch=new StringRequest(Request.Method.POST, urlsearch, new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {
                        Log.e("response",response.toString());
                        Log.e("SHUBHAM","Prasad");
                        students.clear();
                        //               try {
//                    JSONArray jsonArray=new JSONArray(response);
//                    for(int i=0;i<jsonArray.length();i++)
//                    {
//                        JSONObject jsonObject=jsonArray.getJSONObject(i);
//                        date.setText(jsonObject.getString("DATE"));
//                        update.setText(jsonObject.getString("UPDATE"));
//                        updated_By.setText(jsonObject.getString("UPDATED_BY"));
//                        remarks.setText(jsonObject.getString("REMARKS"));
//                        next_person.setText(jsonObject.getString("NEXT_PERSON"));
//                        next_status.setText(jsonObject.getString("NEXT_STATUS"));
//                    }
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
                        try {

                            JSONArray array11=new JSONArray(response);
                            for(int i=0;i<array11.length();i++)
                            {
                                JSONObject jsonObject11=array11.getJSONObject(i);
                                students.add(i,new Sliplistclass(""+jsonObject11.getString("SLIP_NO"),""+jsonObject11.getString("START_DATE"),""+jsonObject11.getString("CLIENT"),""+jsonObject11.getString("STATUS"),""+jsonObject11.getString("REQUIREMENT"),""+jsonObject11.getString("VENDOR_NAME")));
                            }
                            adapter=new Adapter(students,MainActivity.this);
                            recyclerView.setAdapter(adapter);
                            // Sharedpref.getInstance(getApplicationContext()).saveslipno(jsonObject1.getString("token"));




                        } catch (JSONException e) {
                            //  Log.e("sss","hh");
                            e.printStackTrace();
                        }




                    }
                },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Log.e("SHUBHAM",error.toString());

                            }
                        })
                {
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        Log.e("Post",currentstatus+nextperson+nextstatus+txt);

                        Map<String,String> map=new HashMap<>();


                        if(txt.equals("0"))
                        {
                            Log.e("txt","0");
                            map.put("client","");
                        }
                        else
                        {
                            Log.e("client",txt);
                            map.put("client",txt);
                        }

                        if(currentstatus.equals("CURRENT STATUS"))
                        {
                            map.put("current_status","0");
                            Log.e("current_status","0");
                        }
                        else
                        {
                            map.put("current_status",currentstatus);
                            Log.e("current_status",currentstatus);
                        }

                        if(nextstatus.equals("NEXT STATUS"))
                        {

                            map.put("next_status","0");
                            Log.e("next_status","0");
                        }
                        else {
                            Log.e("next_status",nextstatus);
                            map.put("next_status",nextstatus);
                        }

                        if(nextperson.equals("PERSON"))
                        {
                            Log.e("next_person","0");
                            map.put("next_person","0");
                        }
                        else
                        {
                            Log.e("next_person",nextperson);
                            map.put("next_person",nextperson);
                        }


                        return map;
                    }
                };
                mRequestqueuesearch.add(mStringRequestsearch);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        currentstatusspinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                final String currentstatus=currentstatusspinner.getSelectedItem().toString();
                final String nextstatus=nextstatusspinner.getSelectedItem().toString();
                final String nextperson=personspinner.getSelectedItem().toString();

                txt=editText.getText().toString();
                Log.e("Post",currentstatus+nextperson+nextstatus+txt);




                Log.e("Post",currentstatus+nextperson+nextstatus+txt);




                //  RequestQueue mRequestQueue;
                //StringRequest mStringRequest;
                //String url = "http://192.168.43.59/project2/Fetch_Updates.php";
                mRequestqueuesearch = Volley.newRequestQueue(MainActivity.this);
                Log.e("SHUBHAM","Prasad");
                // Log.e("list",listitem.getSlipno());
                mStringRequestsearch=new StringRequest(Request.Method.POST, urlsearch, new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {
                        Log.e("response",response.toString());
                        Log.e("SHUBHAM","Prasad");
                        students.clear();
                        //               try {
//                    JSONArray jsonArray=new JSONArray(response);
//                    for(int i=0;i<jsonArray.length();i++)
//                    {
//                        JSONObject jsonObject=jsonArray.getJSONObject(i);
//                        date.setText(jsonObject.getString("DATE"));
//                        update.setText(jsonObject.getString("UPDATE"));
//                        updated_By.setText(jsonObject.getString("UPDATED_BY"));
//                        remarks.setText(jsonObject.getString("REMARKS"));
//                        next_person.setText(jsonObject.getString("NEXT_PERSON"));
//                        next_status.setText(jsonObject.getString("NEXT_STATUS"));
//                    }
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
                        try {

                            JSONArray array11=new JSONArray(response);
                            for(int i=0;i<array11.length();i++)
                            {
                                JSONObject jsonObject11=array11.getJSONObject(i);
                                students.add(i,new Sliplistclass(""+jsonObject11.getString("SLIP_NO"),""+jsonObject11.getString("START_DATE"),""+jsonObject11.getString("CLIENT"),""+jsonObject11.getString("STATUS"),""+jsonObject11.getString("REQUIREMENT"),""+jsonObject11.getString("VENDOR_NAME")));
                            }
                            adapter=new Adapter(students,MainActivity.this);
                            recyclerView.setAdapter(adapter);
                            // Sharedpref.getInstance(getApplicationContext()).saveslipno(jsonObject1.getString("token"));




                        } catch (JSONException e) {
                            //  Log.e("sss","hh");
                            e.printStackTrace();
                        }




                    }
                },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Log.e("SHUBHAM",error.toString());

                            }
                        })
                {
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        Log.e("Post",currentstatus+nextperson+nextstatus+txt);

                        Map<String,String> map=new HashMap<>();


                        if(txt.equals("0"))
                        {
                            Log.e("txt","0");
                            map.put("client","");
                        }
                        else
                        {
                            Log.e("client",txt);
                            map.put("client",txt);
                        }

                        if(currentstatus.equals("CURRENT STATUS"))
                        {
                            map.put("current_status","0");
                            Log.e("current_status","0");
                        }
                        else
                        {
                            map.put("current_status",currentstatus);
                            Log.e("current_status",currentstatus);
                        }

                        if(nextstatus.equals("NEXT STATUS"))
                        {

                            map.put("next_status","0");
                            Log.e("next_status","0");
                        }
                        else {
                            Log.e("next_status",nextstatus);
                            map.put("next_status",nextstatus);
                        }

                        if(nextperson.equals("PERSON"))
                        {
                            Log.e("next_person","0");
                            map.put("next_person","0");
                        }
                        else
                        {
                            Log.e("next_person",nextperson);
                            map.put("next_person",nextperson);
                        }


                        return map;
                    }
                };
                mRequestqueuesearch.add(mStringRequestsearch);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    recyclerView=(RecyclerView)findViewById(R.id.recyclerid);




        RecyclerView.LayoutManager linearLayoutManager = new LinearLayoutManager(MainActivity.this);
        recyclerView.setLayoutManager(linearLayoutManager);

        Log.e("SHUBHAM","Prasad");


        mRequestQueue = Volley.newRequestQueue(this);
        Log.e("SHUBHAM","Prasad");
        mStringRequest=new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.e("response",response.toString());
                Log.e("SHUBHAM","Prasad");
                try {

                    JSONArray array=new JSONArray(response);
                    for(int i=0;i<array.length();i++)
                    {
                        JSONObject jsonObject1=array.getJSONObject(i);
                        students.add(i,new Sliplistclass(""+jsonObject1.getString("SLIP_NO"),""+jsonObject1.getString("START_DATE"),""+jsonObject1.getString("CLIENT"),""+jsonObject1.getString("STATUS"),""+jsonObject1.getString("REQUIREMENT"),""+jsonObject1.getString("VENDOR_NAME")));
                    }
                    adapter=new Adapter(students,MainActivity.this);
                    recyclerView.setAdapter(adapter);
                   // Sharedpref.getInstance(getApplicationContext()).saveslipno(jsonObject1.getString("token"));




                } catch (JSONException e) {
                  //  Log.e("sss","hh");
                    e.printStackTrace();
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("SHUBHAM",error.toString());

                    }
                });
        mRequestQueue.add(mStringRequest);


        final ArrayList<String> itemsperson = new ArrayList<String>();
        itemsperson.add(0,"PERSON");
        final ArrayAdapter<String> name_adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, itemsperson);
        mRequestqueueperson= Volley.newRequestQueue(this);
        mStringRequestperson=new StringRequest(Request.Method.GET, urlperson, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Log.e("nameresponse",response.toString());

                JSONArray jsonArray1= null;
                try {
                    Sharedpref.getInstance(getApplicationContext()).savearrayperson(response);
                    jsonArray1 = new JSONArray(response);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
               // Log.e("length",String.valueOf(  jsonArray1.length()));
                for(int i=0;i<jsonArray1.length();i++)
                {
                  //  Log.e("jjj","ggg");
                    try {
                         JSONObject jsonObject2 = jsonArray1.getJSONObject(i);
                        itemsperson.add(i+1,jsonObject2.getString("name"));
                        Log.e("rr",jsonObject2.getString("name"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    name_adapter.notifyDataSetChanged();

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("errorname",error.toString());
            }
        });
        mRequestqueueperson.add(mStringRequestperson);

        name_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        personspinner.setAdapter(name_adapter);








        final ArrayList<String> itemscurrent = new ArrayList<String>();
        itemscurrent.add(0,"CURRENT STATUS");
        final ArrayAdapter<String> current_adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, itemscurrent);
        mRequestqueuestatus= Volley.newRequestQueue(this);
        mStringRequeststatus=new StringRequest(Request.Method.GET, urlstatus, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                //Log.e("nameresponse",response.toString());

                JSONArray jsonArray2= null;
                try {
                    Sharedpref.getInstance(getApplicationContext()).savearraycurrent(response);
                    jsonArray2 = new JSONArray(response);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                //Log.e("length",String.valueOf(  jsonArray2.length()));
                for(int i=0;i<jsonArray2.length();i++)
                {
                   // Log.e("jjj","ggg");
                    try {
                        JSONObject jsonObject2 = jsonArray2.getJSONObject(i);
                        itemscurrent.add(i+1,jsonObject2.getString("STATUS"));
                        Log.e("rr",jsonObject2.getString("STATUS"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    current_adapter.notifyDataSetChanged();

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("errorname",error.toString());
            }
        });
        mRequestqueuestatus.add(mStringRequeststatus);

        current_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        currentstatusspinner.setAdapter(current_adapter);
       // nextstatusspinner.setAdapter(current_adapter);




        final ArrayList<String> itemsnext = new ArrayList<String>();
        itemsnext.add(0,"NEXT STATUS");
        final ArrayAdapter<String> next_adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, itemsnext);
        mRequestqueuenext= Volley.newRequestQueue(this);
        mStringRequestnext=new StringRequest(Request.Method.GET, urlstatus, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Log.e("nameresponse",response.toString());

                JSONArray jsonArray3= null;
                try {
                    Sharedpref.getInstance(getApplicationContext()).savearraynext(response);
                    jsonArray3 = new JSONArray(response);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                Log.e("length",String.valueOf(  jsonArray3.length()));
                for(int i=0;i<jsonArray3.length();i++)
                {
                    Log.e("jjj","ggg");
                    try {
                        JSONObject jsonObject3 = jsonArray3.getJSONObject(i);
                        itemsnext.add(i+1,jsonObject3.getString("STATUS"));
                        Log.e("rr",jsonObject3.getString("STATUS"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    name_adapter.notifyDataSetChanged();

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("errorname",error.toString());
            }
        });
        mRequestqueuenext.add(mStringRequestnext);

        name_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        nextstatusspinner.setAdapter(next_adapter);







    }




       public void addslip() {
        AlertDialog.Builder builder11 = new AlertDialog.Builder(MainActivity.this);
        builder11.setTitle("Add Slip");
        View v = this.getLayoutInflater().inflate(R.layout.addsliplayout, null);
        builder11.setView(v);

        current_status=(Spinner)v.findViewById(R.id.addslipcurrentstatus);
        next_status=(Spinner)v.findViewById(R.id.addslipnextstatus);
        current_person=(Spinner)v.findViewById(R.id.addslipcurrenttperson);
        next_person=(Spinner)v.findViewById(R.id.addslipnextperson);

        slipno=(EditText)v.findViewById(R.id.addslipno);
        client=(EditText)v.findViewById(R.id.addslipclient);
        add=(Button) v.findViewById(R.id.addslipbutton);
        date=(TextView)v.findViewById(R.id.addslipdate);
        requirement=(EditText)v.findViewById(R.id.addsliprequirement);
        vendor=(EditText)v.findViewById(R.id.addslipvendor);



        alertDialog11 = builder11.create();
        alertDialog11.show();


        slipno.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                asn=slipno.getText().toString();
            }
        });
        client.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                asc=client.getText().toString();
            }
        });

        //date.setText(simpleDateFormat.format(dateslip));






       // date.setText(simpleDateFormat.format(Calendar.getInstance().getTime()));

//        date.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//
//            }
//
//            @Override
//            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//
//            }
//
//            @Override
//            public void afterTextChanged(Editable editable) {
//                asd=date.getText().toString();
//            }
//        });


        requirement.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                asr=requirement.getText().toString();
            }
        });

        vendor.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                asv=vendor.getText().toString();
            }
        });

        currentstatus.add(0,"CURRENT STATUS");
        ArrayAdapter<String> currentstatus_adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item,currentstatus);
        String cs1=Sharedpref.getInstance(MainActivity.this).getJSONArraycurrent();
        Log.e("currentstatus",cs1);

        try {
            JSONArray array=new JSONArray(cs1);
            for(int i=0;i<array.length();i++)
            {
                JSONObject jsonObject=array.getJSONObject(i);
                currentstatus.add(i+1,jsonObject.getString("STATUS"));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        currentstatus_adapter.notifyDataSetChanged();

        currentstatus_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        current_status.setAdapter(currentstatus_adapter);




        nextstatus.add(0,"NEXT STATUS");
        ArrayAdapter<String> nextstatus_adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item,nextstatus);
        String cs2=Sharedpref.getInstance(MainActivity.this).getJSONArraynext();
        Log.e("next status",cs2);

        try {
            JSONArray array16=new JSONArray(cs2);
            for(int i=0;i<array16.length();i++)
            {
                JSONObject jsonObject16=array16.getJSONObject(i);
                nextstatus.add(i+1,jsonObject16.getString("STATUS"));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        nextstatus_adapter.notifyDataSetChanged();

        nextstatus_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        next_status.setAdapter(nextstatus_adapter);


        currentperson.add(0,"CURRENT PERSON");
        final ArrayAdapter<String> currentperson_adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item,currentperson);
        String cp2=Sharedpref.getInstance(MainActivity.this).getJSONArrayperson();
        String staffid=SharedprefUser.getInstance(this).getStaffId();

        try {
            JSONArray array13=new JSONArray(cp2);
            Log.e("length",String.valueOf(array13.length()));
            for(int i=0;i<array13.length();i++)
            {
                JSONObject jsonObject11=array13.getJSONObject(i);
                //currentp.add(i+1,jsonObject11.getString("name"));

                if(staffid.equals(jsonObject11.getString("staff_id")))
                {
                    currentperson.add(1,jsonObject11.getString("name"));
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        currentperson_adapter.notifyDataSetChanged();

        currentperson_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        current_person.setAdapter(currentperson_adapter);





        nextperson.add(0,"NEXT PERSON");
        final ArrayAdapter<String> nextperson_adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item,nextperson);
        String np1=Sharedpref.getInstance(MainActivity.this).getJSONArrayperson();
        Log.e("np",np1);

        try {
            JSONArray array111=new JSONArray(np1);
            Log.e("length",String.valueOf(array111.length()));
            for(int i=0;i<array111.length();i++)
            {
                JSONObject jsonObject111=array111.getJSONObject(i);
                nextperson.add(i+1,jsonObject111.getString("name"));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        nextperson_adapter.notifyDataSetChanged();

        nextperson_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        next_person.setAdapter(nextperson_adapter);


        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(slipno.getText().toString().length()==0)
                {
                    slipno.setError("Empty Field");
                    return;
                }
                if (client.getText().toString().length()==0)
                {
                    client.setError("Empty Field");
                    return;
                }
                if(date.getText().toString().length()==0)
                {
                    date.setError("Empty Field");
                    return;
                }
                if(requirement.getText().toString().length()==0)
                {
                    requirement.setError("Empty Field");
                    return;
                }

                if(vendor.getText().toString().length()==0)
                {
                    vendor.setError("Empty Field");
                    return;
                }
                final String addslipcs=current_status.getSelectedItem().toString();
                final String addslipns=next_status.getSelectedItem().toString();
                final String addslipcp=current_person.getSelectedItem().toString();
                final String addslipnp=next_person.getSelectedItem().toString();
               // String addslipcs=current_status.getSelectedItem().toString();

                Log.e("currentperson",addslipcp);
                Log.e("nextperson",addslipnp);
                Log.e("nextstatus",addslipns);
                  Log.e("currentstatus",addslipcs);
//
                  Log.e("client",asc);
                  Log.e("require",asr);
                  Log.e("vendor",asv);
                 Log.e("slipno",asn);

                 asd=date.getText().toString();





                mRequestaddslip=Volley.newRequestQueue(MainActivity.this);

                mStringaddslip=new StringRequest(Request.Method.POST, urladdslip, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Toast.makeText(MainActivity.this,"Slip added successfully",Toast.LENGTH_SHORT).show();
                        Log.e("ADDSLIP",response.toString());
                        alertDialog11.dismiss();
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {


                    }
                }){
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        Map<String,String> mp=new HashMap<>();
                        mp.put("slip_no",asn);
                        mp.put("date",asd);
                        mp.put("status",addslipcs);
                        mp.put("require",asr);
                        mp.put("client",asc);
                        mp.put("vendor",asv);
                        mp.put("next_status",addslipns);
                        mp.put("next_person",addslipnp);
                        mp.put("current_person",addslipcp);
                        return mp;
                    }
                };
                mRequestaddslip.add(mStringaddslip);


            }
        });
           try {
               date.setText(new SimpleDateFormat("dd-MM-yyyy").format(new SimpleDateFormat("yyyy-MM-dd").parse(final_selected_date)));
           } catch (ParseException e) {
               e.printStackTrace();
           }

           date.setOnClickListener(new View.OnClickListener() {
               @Override
               public void onClick(View view) {
                   showDialog(0);
                   ids=0;

               }
           });





    }
    @Override
       protected Dialog onCreateDialog(int id) {

           return new DatePickerDialog(this, datepickerlistener, year, month, day);
       }

       Calendar c=Calendar.getInstance();
       int year=c.get(Calendar.YEAR);
       int month=c.get(Calendar.MONTH);
        int day=c.get(Calendar.DAY_OF_MONTH);



       DatePickerDialog.OnDateSetListener datepickerlistener=new DatePickerDialog.OnDateSetListener() {
           @Override
           public void onDateSet(DatePicker datePicker, int selectedyear, int selectedmonth, int selectedday)
           {
               day=selectedday;
               month=selectedmonth;
               year=selectedyear;

               final_selected_date=selectedyear+"-"+(selectedmonth+1)+"-"+selectedday;
               Toast.makeText(getApplicationContext(),"Main",Toast.LENGTH_LONG).show();
               if(ids==0)
               {
                   try {
                       date.setText(new SimpleDateFormat("dd-MM-yyyy").format(new SimpleDateFormat("yyyy-MM-dd").parse(final_selected_date)));
                   } catch (ParseException e) {
                       e.printStackTrace();
                   }
               }
               if(ids==1)
               {
                   try {
                       Adapter.updatedate.setText(new SimpleDateFormat("dd-MM-yyyy").format(new SimpleDateFormat("yyyy-MM-dd").parse(final_selected_date)));
                   } catch (ParseException e) {
                       e.printStackTrace();
                   }
               }
               if(ids==2)
               {
                   try {
                       Adapter.addvendordate.setText(new SimpleDateFormat("dd-MM-yyyy").format(new SimpleDateFormat("yyyy-MM-dd").parse(final_selected_date)));
                   } catch (ParseException e) {
                       e.printStackTrace();
                   }
               }



           }
       };














   }
